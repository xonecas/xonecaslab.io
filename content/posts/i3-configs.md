---
title: "I3 Configs"
date: 2022-01-07T17:07:53Z
draft: false
cover: "/images/i3-cover.png"
---

# Switching to i3 Window Manager

I've ~~recently~~ around a year ago switched to develop full time on a Tuxedo Laptop, and it comes with a bungie desktop environment flavour which is very nice, but after a while I've started to search for alternatives...  I tried a few of the most well known window managers and desktop environments, and ended up using Gnome 3 for the past year.

While Gnome 3 was great in terms of productivity, I was developing and not worrying about setting things up in no time.  For a developer this was a great out of the box configuration.  Although as I used it more and more I started to wish for some tweaks, and after seeing how to develop these tweaks for Gnome, I quickly became discouraged by the API provided and the overall process of making and using these "shell extensions".

This brought me to i3, I never used it before, but I rememeber having a good (for a developer) experience with AwesomeWM and most of the fond memories were related to the level of configuration available and the "everything is just a command" ideology.  After watching some videos, reading through the docs at the i3 website, I was able to arrive in a few hours to what I'm calling the baseline for further configuration.

While things will change considerally in the future, for now the files are available in my [Gitlab](https://gitlab.com/xonecas/i3wm-configurations).



